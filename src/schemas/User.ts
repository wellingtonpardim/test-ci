import { Schema, model, Document } from 'mongoose'

const UserSchema = new Schema({
  email: String,
  firstName: String,
  lastName: String
}, {
  timestamps: true
})

interface User extends Document {
  email: string,
  firstName: string,
  lastName: string,
  fullName(): string
}
UserSchema.methods.fullName = function (): string {
  return this.email + ' ' + this.firstName
}

export default model<User>('User', UserSchema)
